# facial-feature-clustering
Results of the clustering performed on facial features extracted from the Chicago Face Database[1] using the eigenpictures approach and k-means algorithm.

[1] Ma, D. S., Correll, J., & Wittenbrink, B. (2015). The Chicago face database: A free stimulus set of faces and norming data. *Behavior research methods, 47(4)*, 1122-1135.
